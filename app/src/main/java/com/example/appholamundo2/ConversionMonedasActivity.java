package com.example.appholamundo2;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class ConversionMonedasActivity extends AppCompatActivity {

    private EditText editTextCantidad;
    private Spinner spinnerMonedas;
    private TextView textViewResultado;
    private Button buttonCalcular, buttonLimpiar, buttonCerrar;

    private final double tasaUsd = 0.05; // Ejemplo de tasa
    private final double tasaEur = 0.04; // Ejemplo de tasa
    private final double tasaCad = 0.07; // Ejemplo de tasa
    private final double tasaGbp = 0.03; // Ejemplo de tasa

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion_monedas);

        editTextCantidad = findViewById(R.id.editTextCantidad);
        spinnerMonedas = findViewById(R.id.spinnerMonedas);
        textViewResultado = findViewById(R.id.textViewResultado);
        buttonCalcular = findViewById(R.id.buttonCalcular);
        buttonLimpiar = findViewById(R.id.buttonLimpiar);
        buttonCerrar = findViewById(R.id.buttonCerrar);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.monedas_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMonedas.setAdapter(adapter);

        buttonCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularConversion();
            }
        });

        buttonLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        buttonCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calcularConversion() {
        if (!editTextCantidad.getText().toString().isEmpty()) {
            double cantidad = Double.parseDouble(editTextCantidad.getText().toString());
            String monedaSeleccionada = spinnerMonedas.getSelectedItem().toString();
            double resultado = 0;

            switch (monedaSeleccionada) {
                case "Dólares Americanos":
                    resultado = cantidad * tasaUsd;
                    break;
                case "Euros":
                    resultado = cantidad * tasaEur;
                    break;
                case "Dólar Canadiense":
                    resultado = cantidad * tasaCad;
                    break;
                case "Libra Esterlina":
                    resultado = cantidad * tasaGbp;
                    break;
            }

            textViewResultado.setText(String.format("Resultado: %.2f", resultado));
        }
    }

    private void limpiarCampos() {
        editTextCantidad.setText("");
        spinnerMonedas.setSelection(0);
        textViewResultado.setText("");
    }
}